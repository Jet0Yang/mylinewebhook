(function() {

    // 取得設定資料
    const configs = require('../configs.js');

    // 引用寫好的API
    const assistantAPI = require('../services/AssistantAPIV2');
    assistantAPI.init(configs.assistant)

    // 存放 sdk 產生之 session
    let userSessions = {};

    class AssistantController {
        constructor() {
            this.sendMessage = this.sendMessage.bind(this);
        }
        
        // 取得 user session 
        async sendMessage(userId, text) {

			// 取出API所需暫存Session
			//let userSession = userSessions.userId;
			let userSession = userSessions[userId];

            // 若沒有對應session
            if(!userSession){
				// create new session
				let newSession = await assistantAPI.createSession();
				userSession = newSession.session_id;
				userSessions[userId] = userSession;
            }
            
            // 對話
            return assistantAPI.message(text, userSession)
            .then((result) => {
                // 整理過Assistant(回覆json格式)後，僅需要作 
                console.log(result.output.generic[0].text);
                return JSON.parse(result.output.generic[0].text);
                // VA設定回傳內容使用 --> { "type": "text", "text": "您好，有什麼能為您服務的地方？" }
                // JSON.parse轉換成JSON格式 --> { type: 'text', text: '您好，有什麼能為您服務的地方？' }

                // 未整理Assistant(回覆原始文字)使用這一區
                // return {
                //     type: 'text',
                //     text: result.output.generic[0].text
                // };
            }).catch((err) => {
                console.log(err)
                return 'fail to call assistant';
            });
        }
    }

    module.exports = new AssistantController();
}());