let express = require('express');
let router = express.Router();

const AssistantCtl = require('../Controller/AssistantController');
const Logger = require('../Controller/Logger');
const MongodbCtl = require('../Controller/MongodbController');

// line sdk
const line = require('@line/bot-sdk');

// 取得設定資料
const configs = require('../configs.js');

// line bot channel access token
const channelAccessToken = configs.channelAccessToken;

// init a client 
const client = new line.Client({
	channelAccessToken: channelAccessToken
});
// init Logger
Logger.init('webhook');
// init Mongodb
MongodbCtl.init();


router.post('/line', async function(req, res, next) {

	Logger.log('webhook received an event');
    res.status(200).send();
	
	let targetEvent = req.body.events[0];
	if(targetEvent.type == 'message'){
		
		if(targetEvent.message.type == 'text'){

			// 取出 userId & user 説的文字
			let userId = targetEvent.source.userId,
				userSay = targetEvent.message.text;

			// logger.log(userId, 'says:', userSay);

			// 取得回覆
      		let result = await AssistantCtl.sendMessage(userId, userSay);
      		// console.log(result)
			
			// 將回覆傳回給user
			replyToLine(targetEvent.replyToken, result);
			Logger.log(userId, 'return message:', result);
      
      		//紀錄DB
      		let document = {
        		timestamp:Date.now(),
        		userId:userId,
        		userSay:userSay,
        		AssistantReturn:result
      		}
      		MongodbCtl.insertOne('LineBot','chatlog',document)
		}
	}
});

module.exports = router;


function replyToLine(rplyToken, message) {

	return client.replyMessage(rplyToken, message)
	.then((res) => {
		// console.log(res)
		return true; 
	})
	.catch((err) => {
		// console.log(err)
		return false;
	});
}
